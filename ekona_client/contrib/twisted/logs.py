# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import os
import threading
from collections import deque
from datetime import datetime, timezone
from urllib.parse import urlparse

from twisted.application import internet
from twisted.application.service import MultiService
from twisted.internet import reactor, ssl
from twisted.internet.protocol import Protocol, ReconnectingClientFactory, Factory, connectionDone
from twisted.logger import Logger
from twisted.python import log as legacyLog
from twisted.python.failure import Failure
from twisted.python.log import ILogObserver
from zope.interface import implementer, Interface

from ekona_client.contrib.module_loading import import_string

LOG_EVENT_TRANSFORMER = os.getenv('LOG_EVENT_TRANSFORMER', None)
LOG_EVENT_FORMATTER = os.getenv('LOG_EVENT_FORMATTER', None)
LOG_FORWARDER = os.getenv('LOG_FORWARDER', None)
LOG_TO_LEGACY_CONSOLE = os.getenv('LOG_TO_LEGACY_CONSOLE', '0').lower() in ['1', 'true']

DD_API_KEY = os.getenv('DD_API_KEY', None)


class ILogEventTransformer(Interface):
    def transformLogEvent(self, eventDict: dict) -> dict:
        pass


class ILogEventFormatter(Interface):
    def formatLogEvent(self, eventDict: dict) -> str:
        pass


@implementer(ILogEventTransformer)
class DatadogLogEventTransformer(object):
    def transformLogEvent(self, eventDict: dict) -> dict:
        result = {
            'message': eventDict['log_format'].format(**eventDict),
            'logger.name': eventDict['log_namespace'],
            'logger.thread_name': threading.current_thread().name,
            'timestamp': datetime.fromtimestamp(eventDict['log_time'], tz=timezone.utc).isoformat(),
            'level': eventDict['log_level'].name.lower(),
            'source': 'python'
        }

        fail = eventDict.get('log_failure', None)
        if fail is not None:
            result.update({
                'error.message': fail.getErrorMessage(),
                'error.stack': fail.getTraceback(),
                'error.kind': fail.type.__name__
            })

        return result


@implementer(ILogEventFormatter)
class JsonLogEventFormatter(object):
    def formatLogEvent(self, eventDict: dict) -> str:
        return json.dumps(eventDict) + '\n'


class DatadogJsonLogEventFormatter(JsonLogEventFormatter):
    def formatLogEvent(self, eventDict: dict) -> str:
        if DD_API_KEY is None:
            raise Exception('DatadogJsonLogEventFormatter.write: DD_API_KEY is not set!')
        return DD_API_KEY + ' ' + json.dumps(eventDict) + '\n'


@implementer(ILogEventTransformer, ILogEventFormatter, ILogObserver)
class BaseStreamingLogObserver(object):
    logToConsoleLegacy = False
    logEventTransformer = None  # type: ILogEventTransformer
    logEventFormatter = None  # type: ILogEventFormatter

    def __init__(self):
        if LOG_EVENT_TRANSFORMER is not None:
            print('LOG_EVENT_TRANSFORMER: {LOG_EVENT_TRANSFORMER}'.format(LOG_EVENT_TRANSFORMER=LOG_EVENT_TRANSFORMER))
            self.logEventTransformer = import_string(LOG_EVENT_TRANSFORMER)()

        if LOG_EVENT_FORMATTER is not None:
            print('LOG_EVENT_FORMATTER: {LOG_EVENT_FORMATTER}'.format(LOG_EVENT_FORMATTER=LOG_EVENT_FORMATTER))
            self.logEventFormatter = import_string(LOG_EVENT_FORMATTER)()

    def transformLogEvent(self, eventDict: dict) -> dict:
        if self.logEventTransformer is not None:
            return self.logEventTransformer.transformLogEvent(eventDict=eventDict)

        # By default, return untouched eventDict
        return eventDict

    def formatLogEvent(self, eventDict: dict) -> str:
        """
        {
            'log_logger': <Logger 'twisted.plugins.xovis_push_plugin'>,
            'log_level': <LogLevel=info>,
            'log_namespace': 'twisted.plugins.xovis_push_plugin',
            'log_source': None,
            'log_format': 'TEST LOG',
            'log_time': 1539031181.4076085
        }
        """
        if self.logEventFormatter is not None:
            return self.logEventFormatter.formatLogEvent(eventDict=eventDict)

        # By default, format "log_format", and append a "\n"
        return eventDict['log_format'] % eventDict + '\n'

    def write(self, text: bytes, isError: bool=False) -> None:
        raise NotImplementedError

    def __call__(self, eventDict: dict) -> None:
        eventDict = self.transformLogEvent(eventDict)
        text = self.formatLogEvent(eventDict)

        if self.logToConsoleLegacy:
            if eventDict.get('isError', False) is False:
                legacyLog.msg(text)
            else:
                legacyLog.err(text)

        if text is None:
            text = ''

        if text:
            self.write(text.encode('utf-8'), isError=eventDict.get('isError', False))


class TCPLogForwarderProtocol(Protocol):
    def connectionMade(self):
        self.factory.connectionMade(self)

    def connectionLost(self, reason=connectionDone):
        self.factory.connectionLost(self, reason=connectionDone)


class TCPLogForwarderFactory(ReconnectingClientFactory):
    protocol = TCPLogForwarderProtocol  # type: TCPLogForwarderProtocol

    def __init__(self, queueSize=1000):
        ReconnectingClientFactory.__init__(self)

        self._protocol = None
        self.forwarderQueue = deque(maxlen=queueSize)

    def startedConnecting(self, connector):
        legacyLog.msg('TCPLogForwarderFactory.startedConnecting: connector={connector}'.format(connector=connector))

    def connectionMade(self, protocol: TCPLogForwarderProtocol):
        legacyLog.msg('TCPLogForwarderFactory.connectionMade: {protocol}'.format(protocol=protocol))
        self._protocol = protocol

        reactor.callLater(5, self._flushLogQueue)

    def connectionLost(self, protocol: TCPLogForwarderProtocol, reason: Failure = connectionDone):
        legacyLog.msg('TCPLogForwarderFactory.connectionLost: {protocol}, {reason}'.format(
            protocol=protocol, reason=reason))
        self._protocol = None

    def clientConnectionLost(self, connector, reason):
        legacyLog.msg('TCPLogForwarderFactory.clientConnectionLost: lost connection.  Reason:', reason)
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)
        self._protocol = None

    def clientConnectionFailed(self, connector, reason):
        legacyLog.msg('TCPLogForwarderFactory.clientConnectionFailed: connection failed. Reason:', reason)
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)
        self._protocol = None

    def queueLogData(self, data):
        self.forwarderQueue.append(data)

        if not self._protocol:
            return

        self._flushLogQueue()

    def _flushLogQueue(self):
        if not self._protocol:
            return
        while self.forwarderQueue:
            data = self.forwarderQueue.popleft()
            self._write(data)

    def _write(self, data):
        self._protocol.transport.write(data)


class TCPLogForwarder(BaseStreamingLogObserver):
    factory = TCPLogForwarderFactory  # type: TCPLogForwarderFactory

    def __init__(self, host, port, secure=False):
        BaseStreamingLogObserver.__init__(self)

        legacyLog.msg('TCPLogForwarderFactory.__init__: host: {host}, port: {port}, secure: {secure}'.format(
            host=host, port=port, secure=secure))

        self._factory = self.factory()

        if not secure:
            reactor.connectTCP(host, port, self._factory)
        else:
            reactor.connectSSL(host, port, self._factory, ssl.ClientContextFactory())

    def write(self, text: bytes, isError: bool = False) -> None:
        self._factory.queueLogData(data=text)


# class TCPLogBrokerProtocol(Protocol):
#     def connectionMade(self):
#         self.factory.connectionMade(self)
#
#     def connectionLost(self, reason=connectionDone):
#         self.factory.connectionLost(self, reason=connectionDone)
#
#     def write(self, text: bytes, isError: bool = False):
#         self.transport.write(text)
#
#
# class TCPLogBrokerFactory(Factory):
#     protocol = TCPLogBrokerProtocol
#     connections = []  # type: [TCPLogBrokerProtocol]
#
#     def connectionMade(self, protocol: TCPLogBrokerProtocol):
#         legacyLog.msg('TCPLogBrokerFactory.connectionMade: {protocol}'.format(protocol=protocol))
#         if protocol not in self.connections:
#             self.connections.append(protocol)
#
#     def connectionLost(self, protocol: TCPLogBrokerProtocol, reason: Failure = connectionDone):
#         legacyLog.msg('TCPLogBrokerFactory.connectionLost: {protocol}, {reason}'.format(
#             protocol=protocol, reason=reason))
#         if protocol in self.connections:
#             self.connections.remove(protocol)
#
#     def write(self, text: bytes):
#         for connection in self.connections:
#             connection.transport.write(text)


class TCPLogQueueProducerProtocol(Protocol):
    def dataReceived(self, data):
        self.factory.service.handleLogData(data)


class TCPLogQueueProducerFactory(Factory):
    name = 'tcp-log-consumerQueue-producer'
    service = None  # type: TCPLogRouter
    protocol = TCPLogQueueProducerProtocol  # type: TCPLogQueueProducerProtocol


class TCPLogQueueConsumerProtocol(Protocol):
    def connectionMade(self):
        self.factory.connectionMade(self)

    def connectionLost(self, reason=connectionDone):
        self.factory.connectionLost(self, reason=connectionDone)


class TCPLogQueueConsumerFactory(Factory):
    name = 'tcp-log-consumerQueue-consumer'
    service = None  # type: TCPLogRouter
    protocol = TCPLogQueueConsumerProtocol  # type: TCPLogQueueConsumerProtocol
    connections = []  # type: [TCPLogQueueConsumerProtocol]

    def __init__(self, queueSize):
        Factory.__init__(self)

        self.consumerQueue = deque(maxlen=queueSize)

    def connectionMade(self, protocol: TCPLogQueueConsumerProtocol):
        legacyLog.msg('TCPLogQueueConsumerFactory.connectionMade: {protocol}'.format(protocol=protocol))
        if protocol not in self.connections:
            self.connections.append(protocol)

        reactor.callLater(5, self._flushLogQueue)

    def connectionLost(self, protocol: TCPLogQueueConsumerProtocol, reason: Failure = connectionDone):
        legacyLog.msg('TCPLogQueueConsumerFactory.connectionLost: {protocol}, {reason}'.format(
            protocol=protocol, reason=reason))
        if protocol in self.connections:
            self.connections.remove(protocol)

    def queueLogData(self, data):
        self.consumerQueue.append(data)

        if not self.connections:
            return

        self._flushLogQueue()

    def _flushLogQueue(self):
        if self.connections:
            while self.consumerQueue:
                data = self.consumerQueue.popleft()
                self._write(data)

    def _write(self, data):
        for connection in self.connections:
            connection.transport.write(data)


class TCPLogRouter(MultiService):
    producerFactory = TCPLogQueueProducerFactory
    consumerFactory = TCPLogQueueConsumerFactory

    def __init__(self, producerPort, consumerPort, queueSize=10000):
        MultiService.__init__(self)

        self._producerFactory = self.producerFactory()
        self._producerFactory.service = self
        self.addService(internet.TCPServer(producerPort, self._producerFactory))

        self._consumerFactory = self.consumerFactory(queueSize=queueSize)
        self._consumerFactory.service = self
        self.addService(internet.TCPServer(consumerPort, self._consumerFactory))

    def handleLogData(self, data):
        self._consumerFactory.queueLogData(data)


def buildLogForwarderFromEnv():
    if LOG_FORWARDER is None:
        return None

    p = urlparse(LOG_FORWARDER)
    if p.scheme in ('tcp', 'ssl'):
        host = p.hostname or 'localhost'
        port = p.port or 10518
        secure = p.scheme == 'ssl'
        return TCPLogForwarder(host=host, port=port, secure=secure)


__observer = buildLogForwarderFromEnv()
if __observer is not None:
    __observer.logToConsoleLegacy = LOG_TO_LEGACY_CONSOLE


def getLogger(namespace=None, source=None, observer=None):
    return Logger(namespace=namespace, source=source, observer=observer or __observer)
