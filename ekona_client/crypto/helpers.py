# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from cryptography.hazmat.primitives.asymmetric.x25519 import X25519PrivateKey, X25519PublicKey
from cryptography.hazmat.primitives.ciphers.aead import ChaCha20Poly1305
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography import exceptions as cryptography_exceptions
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat, PrivateFormat, load_der_public_key, \
    load_pem_public_key, NoEncryption, load_der_private_key, load_pem_private_key, BestAvailableEncryption


class InvalidSignature(Exception):
    pass


def assert_bytes(bytes_or_memoryview):
    return bytes(bytes_or_memoryview) if bytes_or_memoryview is not None else None


class KeyDerivationHelper(object):
    SALT_LENGTH = 16

    def __init__(self, length=32):
        self.length = length

    def derive(self, data):
        salt = os.urandom(self.SALT_LENGTH)
        result = self.get_kdf(salt).derive(data)
        return salt + result

    def verify(self, data, signature):
        salt = signature[:self.SALT_LENGTH]
        signature = signature[self.SALT_LENGTH:]
        return self.get_kdf(salt).verify(data, assert_bytes(signature))

    def get_kdf(self, salt):
        return Scrypt(
            salt=assert_bytes(salt),
            length=self.length,
            n=2**14,
            r=8,
            p=1,
            backend=default_backend())


class SignatureHelper(object):
    PRIVATE_KEY_SIZE = 2048
    HASH_ALGORITHM_CLASS = hashes.SHA256
    PK_ENCODING = Encoding.DER

    def __init__(self, private_key_obj=None):
        self.private_key_obj = private_key_obj or rsa.generate_private_key(
            public_exponent=65537, key_size=self.PRIVATE_KEY_SIZE, backend=default_backend())

    def get_public_key(self):
        return self.private_key_obj.public_key().public_bytes(self.PK_ENCODING, PublicFormat.SubjectPublicKeyInfo)

    def get_private_key(self, password=None):
        if not password:
            private_key_bytes = self.private_key_obj.private_bytes(
                self.PK_ENCODING, PrivateFormat.PKCS8, NoEncryption())
        else:
            private_key_bytes = self.private_key_obj.private_bytes(
                self.PK_ENCODING, PrivateFormat.PKCS8, BestAvailableEncryption(password=password))
        return private_key_bytes

    def sign(self, data):
        return self.private_key_obj.sign(data, self._get_padding(), self.HASH_ALGORITHM_CLASS())

    @classmethod
    def _get_padding(cls, hash_algorithm=None):
        hash_algorithm = hash_algorithm or cls.HASH_ALGORITHM_CLASS()
        return padding.PSS(mgf=padding.MGF1(hash_algorithm), salt_length=padding.PSS.MAX_LENGTH)

    @classmethod
    def load(cls, private_key, password=None, pk_encoding=None):
        pk_encoding = pk_encoding or cls.PK_ENCODING

        if pk_encoding == Encoding.DER:
            private_key_obj = load_der_private_key(
                assert_bytes(private_key), password=password, backend=default_backend())
        elif pk_encoding == Encoding.PEM:
            private_key_obj = load_pem_private_key(
                assert_bytes(private_key), password=password, backend=default_backend())
        else:
            raise ValueError('Unsupported encoding: {}'.format(pk_encoding))

        return cls(private_key_obj=private_key_obj)

    @classmethod
    def verify(cls, public_key, signature, data, hash_algorithm=None, pk_encoding=None):
        """

        :param six.string_types public_key:
        :param six.string_types signature:
        :param six.binary_types data:
        :param hash_algorithm:
        :param pk_encoding:
        :return:
        """
        pk_encoding = pk_encoding or cls.PK_ENCODING
        hash_algorithm = hash_algorithm or cls.HASH_ALGORITHM_CLASS()

        if pk_encoding == Encoding.DER:
            public_key_obj = load_der_public_key(assert_bytes(public_key), backend=default_backend())
        elif pk_encoding == Encoding.PEM:
            public_key_obj = load_pem_public_key(assert_bytes(public_key), backend=default_backend())
        else:
            raise ValueError('Unsupported encoding: {}'.format(pk_encoding))

        try:
            public_key_obj.verify(
                signature,
                data,
                cls._get_padding(hash_algorithm=hash_algorithm),
                hash_algorithm)
        except cryptography_exceptions.InvalidSignature as e:
            raise InvalidSignature(e)


class HandshakeHelper(object):
    NONCE_LENGTH = 12
    AAD_SALT_LENGTH = 4
    AAD_LENGTH = 24

    def __init__(self):
        self.private_key = X25519PrivateKey.generate()

    def get_public_key(self):
        return self.private_key.public_key().public_bytes()

    def get_shared_key(self, peer_public_key):
        peer_public_key_bytes = peer_public_key
        return self.private_key.exchange(X25519PublicKey.from_public_bytes(peer_public_key_bytes))

    def encrypt(self, peer_public_key, plain_data):
        shared_key = self.get_shared_key(peer_public_key)

        key = HKDF(
            algorithm=hashes.SHA256(),
            length=32,
            salt=None,
            info=b'encryption-key',
            backend=default_backend()).derive(shared_key)

        nonce = os.urandom(self.NONCE_LENGTH)

        aad_salt = os.urandom(self.AAD_SALT_LENGTH)

        aad = HKDF(
            algorithm=hashes.SHA256(),
            length=self.AAD_LENGTH,
            salt=aad_salt,
            info=plain_data,
            backend=default_backend()).derive(shared_key)

        result = ChaCha20Poly1305(key).encrypt(nonce, plain_data, aad)
        return nonce + aad_salt + aad + result

    def decrypt(self, peer_public_key, encrypted_data):
        shared_key = self.get_shared_key(peer_public_key)

        key = HKDF(
            algorithm=hashes.SHA256(),
            length=32,
            salt=None,
            info=b'encryption-key',
            backend=default_backend()).derive(shared_key)

        nonce = encrypted_data[:self.NONCE_LENGTH]

        aad_salt = encrypted_data[self.NONCE_LENGTH:self.NONCE_LENGTH + self.AAD_SALT_LENGTH]

        aad = encrypted_data[
            self.NONCE_LENGTH + self.AAD_SALT_LENGTH:self.NONCE_LENGTH + self.AAD_SALT_LENGTH + self.AAD_LENGTH]

        encrypted_data = encrypted_data[self.NONCE_LENGTH + self.AAD_SALT_LENGTH + self.AAD_LENGTH:]

        plain_data = ChaCha20Poly1305(key).decrypt(nonce, encrypted_data, aad)

        # Verify aad
        hkdf = HKDF(
            algorithm=hashes.SHA256(),
            length=self.AAD_LENGTH,
            salt=aad_salt,
            info=plain_data,
            backend=default_backend())
        hkdf.verify(shared_key, aad)

        return plain_data
