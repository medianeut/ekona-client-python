# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import pytz
from datetime import datetime

from ekona_client import settings

utc = pytz.utc


def now() -> datetime:
    if settings.USE_TZ:
        # timeit shows that datetime.now(tz=utc) is 24% slower
        return datetime.utcnow().replace(tzinfo=utc)
    else:
        return datetime.now()
