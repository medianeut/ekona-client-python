#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from ekona_client/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("ekona_client", "__init__.py")


if sys.argv[-1] == 'publish':
    try:
        import wheel
        print("Wheel version: ", wheel.__version__)
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version on git:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='ekona-client',
    version=version,
    description="""A python client SDK for the Ekona Platform""",
    long_description=readme + '\n\n' + history,
    author='Dirk Moors',
    author_email='technology@nurama.com',
    url='https://bitbucket.org/medianeut/ekona-client-python',
    packages=[
        'ekona_client',
    ],
    include_package_data=True,
    install_requires=[
        'cryptography>=2.3,<3.0',
        'six>=1.0.0',
        'pytz>=2018.1',
        'python-dateutil>=2.0.0',
        'datadog>=0.22.0',
        'raven>=6.6.0'
    ],
    extras_require = {
        'twisted':  [
            "Twisted>=17.0.0,<19.0.0",
            "treq>=17.8.0,<18.0.0"
        ]
    },
    license="MIT",
    zip_safe=False,
    keywords='ekona-client',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
)
