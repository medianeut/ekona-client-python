============
ekona-client
============

A python client SDK for the Ekona Platform

Documentation
-------------

The full documentation is at https://ekona-client.readthedocs.io.

Quickstart
----------

Install ekona-client::

    pip install ekona-client


Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox
