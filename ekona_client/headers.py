# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import hashlib
import six
import uuid

from datetime import timedelta
from dateutil.parser import parse as dateparse
from ekona_client.contrib.logs import getLogger

from . import settings, crypto, devices
from .contrib import encoding, timezone

logger = getLogger(__name__)


def get_signature_data(device_type: str, device_guid: uuid.UUID, request_timestamp_str: str, host: str, path: str,
                       query: str, body: str, content_type: str) -> str:
    logger.debug(
        'get_signature_data: device_type: {device_type}, device_guid: {device_guid}, '
        'request_timestamp_str: {request_timestamp_str}, host: {host}, path: {path}, query: {query}, body: {body}, '
        'content_type: {content_type}'.format(
            device_type=device_type, device_guid=device_guid, request_timestamp_str=request_timestamp_str, host=host,
            path=path, query=query, body=body, content_type=content_type))

    m = hashlib.sha256()

    for value in [content_type, host, path, query, request_timestamp_str,
                  '{}:{}'.format(device_type, device_guid), body]:
        if not value:
            continue
        m.update(value.encode('utf-8'))
    return m.hexdigest().encode('utf-8')


def get_device_headers(device_type: str, device_guid: uuid.UUID, device_signature_helper: crypto.SignatureHelper,
                       host: str, path: str, query: str=None, body: str=None, content_type: str=None,
                       timestamp_provider: callable=None) -> dict:
    """
    The method generates signed device-guid headers

    :param device_type: Atm, either 'player' or 'sensor-gateway'
    :param device_guid:
    :param device_signature_helper:
    :param content_type:
    :param host:
    :param path:
    :param query:
    :param body:
    :param content_type:
    :param timestamp_provider:
    :return:
    """
    # Define request timestamp (eg: "Wed, 19 Apr 2017 15:31:24 GMT")
    timestamp_provider = timestamp_provider or timezone.now
    request_timestamp_str = timestamp_provider().strftime(settings.DATE_HEADER_TIMESTAMP_FMT)

    # Define content-type
    content_type = content_type or settings.DEFAULT_CONTENT_TYPE

    # # Define body
    # body = body or ''
    # if isinstance(body, six.binary_type):
    #     body = body.decode('utf-8')

    # Define data to sign
    signature_data = get_signature_data(
        device_type=device_type, device_guid=device_guid, request_timestamp_str=request_timestamp_str, host=host,
        path=path, query=query, body=body, content_type=content_type)

    # Hexlify client public key
    client_public_key = encoding.hexlify_string(device_signature_helper.get_public_key())

    # Make signature
    original_signature = device_signature_helper.sign(signature_data)

    # Hexlify signature
    data_signature = encoding.hexlify_string(original_signature)

    # Merge parts together
    signature = '{client_public_key}{split_char}{data_signature}'.format(
        client_public_key=client_public_key, data_signature=data_signature,
        split_char=settings.SIGNED_HEADER_SPLIT_CHAR)

    # Define header dict
    return {
        'Date': request_timestamp_str,
        'X-EkonaDevice-Id': six.text_type(device_guid),
        'X-EkonaDevice-Type': device_type,
        'X-EkonaDevice-Authorization': signature
    }


def verify_device_authorization(device_registry: devices.AbstractDeviceLibraryRegistry, device_type: str,
                                device_guid: uuid.UUID, device_auth_signature: str, request_timestamp_str: str,
                                host: str, path: str, query: str=None, body: str=None,
                                content_type: str=None) -> object:
    SIGNED_MESSAGE_TIMEOUT_SECONDS = getattr(settings, 'SIGNED_MESSAGE_TIMEOUT_SECONDS', 10)

    # Parse request timestamp
    request_timestamp = dateparse(request_timestamp_str)
    timeout = SIGNED_MESSAGE_TIMEOUT_SECONDS
    if timezone.now() > request_timestamp + timedelta(seconds=timeout):
        logger.warning('verify_device_authorization: expired request timestamp.')
        return

    # Verify authorization header format
    device_authorization_parts = device_auth_signature.split(settings.SIGNED_HEADER_SPLIT_CHAR)
    if len(device_authorization_parts) != 2:
        logger.warning('verify_device_authorization: invalid authorization header format.')
        return

    # Retrieve the device
    registry = device_registry.get(device_type)
    device = registry.get_device(guid=device_guid)
    if device is None:
        logger.warning('verify_device_authorization: device does not exist: {}:{}'.format(device_type, device_guid))
        return

    # Make sure the device is active
    if not device.is_active():
        logger.warning('verify_device_authorization: device is not active')
        return

    # Split authorization header into client_public_key and signature
    client_public_key, signature = device_authorization_parts

    # Compare public key
    if encoding.hexlify_string(device.get_signature_public_key()) != client_public_key:
        logger.warning('verify_device_authorization: invalid public key.')
        return

    # Unhexlify parts
    signature = encoding.unhexlify_string(signature)

    # Define content-type
    content_type = content_type or settings.DEFAULT_CONTENT_TYPE

    # Define body
    body = body or ''
    if isinstance(body, six.binary_type):
        body = body.decode('utf-8')

    # Define data to sign
    signature_data = get_signature_data(
        device_type=device_type, device_guid=device_guid, request_timestamp_str=request_timestamp_str, host=host,
        path=path, query=query, body=body, content_type=content_type)

    # Verify the timestamp signature
    try:
        crypto.SignatureHelper.verify(device.get_signature_public_key(), signature, signature_data)
    except crypto.InvalidSignature as e:
        logger.warning('verify_device_authorization: invalid signature: {}'.format(e))
        return

    # Verification successful. Return the device!
    return device

