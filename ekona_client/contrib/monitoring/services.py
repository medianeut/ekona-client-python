# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from twisted.application.service import Service
from twisted.internet.defer import succeed
from twisted.internet.task import LoopingCall
from twisted.internet.threads import deferToThread


class Monitor(object):
    def __init__(self, fnc, interval, in_thread=False):
        self.fnc = fnc
        self.interval = interval

        def wrap_fnc():
            if in_thread:
                return deferToThread(self.fnc)
            return succeed(self.fnc())

        self._lc = LoopingCall(wrap_fnc)

    def start(self):
        if self._lc and not self._lc.running:
            self._lc.start(self.interval, now=True)

    def stop(self):
        if self._lc and self._lc.running:
            self._lc.stop()


class MonitorService(Service):
    def __init__(self):
        self.monitors = []

    def startService(self):
        Service.startService(self)

        for monitor in self.monitors:
            monitor.start()

    def stopService(self):
        for monitor in self.monitors:
            monitor.stop()

        Service.stopService(self)

    def register(self, interval, in_thread=False):
        def true_decorator(f):
            self.monitors.append(Monitor(fnc=f, interval=interval, in_thread=in_thread))
        return true_decorator
