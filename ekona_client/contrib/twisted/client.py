# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import shelve
import urllib.parse

import re
import os
import uuid
from datetime import timedelta
from six import with_metaclass
from abc import ABCMeta, abstractmethod

from treq.content import json_content
from twisted.application.service import Service
from twisted.internet import reactor
from twisted.internet.defer import ensureDeferred
from twisted.internet.task import LoopingCall
from twisted.web.client import Agent

from ekona_client import crypto
from ekona_client.contrib import encoding, timezone
from ekona_client.contrib.logs import getLogger

DEFAULT_API_URL = 'https://app.nurama.com/api/v3/'
DEFAULT_DATA_FILE = '.ekona-client-data'

logger = getLogger(__name__)


class AbstractDeviceIdProvider(with_metaclass(ABCMeta, object)):
    @abstractmethod
    def get_device_id(self):
        pass


class DefaultDeviceIdProvider(AbstractDeviceIdProvider):
    def get_device_id(self):
        # Source: https://www.geeksforgeeks.org/extracting-mac-address-using-python/
        return ':'.join(re.findall('..', '%012x' % uuid.getnode()))


class EkonaClientBase(object):
    deviceReady = False

    def __init__(self, device_type, data_file: str=None):
        self._device_type = device_type
        self._data_file = data_file or DEFAULT_DATA_FILE

        self._device_guid = None
        self._device_name = None

        # Make sure the datafile directory exists
        self._ensure_data_file_root()

        # Try to load from db
        self._load_from_db()

    @property
    def device_type(self):
        return self._device_type

    @property
    def device_guid(self):
        return self._device_guid

    @property
    def device_name(self):
        return self._device_name

    def _ensure_data_file_root(self):
        data_file_root = os.path.dirname(self._data_file)
        if not os.path.isdir(data_file_root):
            os.makedirs(data_file_root)

    def _load_from_db(self):
        with shelve.open(self._data_file) as db:
            if 'guid' in db:
                self._device_guid = db['guid']
                self._device_name = db['name']
                self.deviceReady = True


class EkonaClient(EkonaClientBase):
    signature_helper = None
    handshake_helper = None
    provision_retry_interval = 10
    provisioning_state_poll_interval = 10

    def __init__(self, device_type: str, client_id: str, client_secret: str, api_url: str=None, data_file: str=None,
                 device_id_provider: AbstractDeviceIdProvider=None):
        EkonaClientBase.__init__(self, device_type=device_type, data_file=data_file)

        from .api import EkonaClientTreqWrapper

        self._client_id = client_id
        self._client_secret = client_secret
        self._api_url = api_url or DEFAULT_API_URL
        self._device_id_provider = device_id_provider or DefaultDeviceIdProvider()

        self._treq = EkonaClientTreqWrapper(self)

        self._provisioning_request_guid = None
        self._provisioning_poll_in_progress = False
        self._provisioning_poll_loop = LoopingCall(lambda: ensureDeferred(self._poll_provisioning_state()))

        # Load signature helper
        try:
            self.signature_helper = self._load_signature_helper()
        except ImportError as e:
            import traceback
            traceback.print_exc()
            print('EkonaClient: Import error: {}'.format(e))

    def onChallenge(self, challenge):
        logger.debug('onChallenge: {challenge}', challenge=challenge)

    def onDeviceReady(self):
        raise NotImplementedError

    def buildMetadata(self):
        return {}

    def provision(self):
        ensureDeferred(self._provision())

    def stop_provisioning(self):
        if self._provisioning_poll_loop and self._provisioning_poll_loop.running:
            self._provisioning_poll_loop.stop()

    @property
    def treq(self):
        return self._treq

    @property
    def api_url(self):
        return self._api_url

    @property
    def challenge(self):
        _challenge = None
        with shelve.open(self._data_file) as db:
            if 'challenge' in db:
                _challenge = db['challenge']
        return _challenge

    def _load_signature_helper(self) -> crypto.SignatureHelper:
        with shelve.open(self._data_file) as db:
            if 'signature' not in db:
                signature_helper = crypto.SignatureHelper()
                private_key = signature_helper.get_private_key()

                db['signature'] = {
                    'client_privkey': encoding.hexlify_string(private_key)
                }
                logger.debug('created new signature: {}'.format(signature_helper))
            else:
                private_key = encoding.unhexlify_string(db['signature']['client_privkey'])

                signature_helper = crypto.SignatureHelper.load(private_key)
                logger.debug('loaded existing signature: {}'.format(signature_helper))

            return signature_helper

    async def get_or_request_access_token(self):
        cached_access_token = None
        with shelve.open(self._data_file) as db:
            if 'token' in db:
                access_token_expires = db['token_expires']
                if access_token_expires < timezone.now():
                    del db['token']
                    del db['token_expires']
                else:
                    cached_access_token = db['token']
        if cached_access_token:
            return cached_access_token

        # Make sure we have client credentials
        if not all([self._client_id, self._client_secret]):
            raise Exception('Missing client credentials.')

        # Prepare request body
        token_request_data = {
            'grant_type': 'client_credentials',
        }

        # Perform requests
        response = await self.treq.post(self._build_api_url('oauth/token/'), data=token_request_data,
                                   auth=(self._client_id, self._client_secret),
                                   agent=Agent(reactor))
        assert response.code == 200, 'get_or_request_access_token: response code: {}, response: {}'.format(
            response.code, response)

        # Retrieve token from response
        data = await json_content(response=response)
        access_token = data['access_token']
        assert access_token is not None

        access_token_expires = timezone.now() + timedelta(seconds=data['expires_in'])

        # Store token
        with shelve.open(self._data_file) as db:
            db['token'] = access_token
            db['token_expires'] = access_token_expires

        logger.debug('received new access token: {access_token}', access_token=access_token)
        return access_token

    def _setDeviceReady(self):
        self.deviceReady = True
        self.onDeviceReady()

    async def _provision(self):
        with shelve.open(self._data_file) as db:
            if 'guid' in db:
                if 'challenge' in db:
                    del db['challenge']
                self._device_guid = db['guid']
                self._device_name = db['name']
                self._setDeviceReady()
                return
            elif 'challenge' in db:
                self.onChallenge(db['challenge'])

        # Get access token
        access_token = await self.get_or_request_access_token()
        assert access_token is not None

        # Define client handshake helper
        self.handshake_helper = crypto.HandshakeHelper()

        # Create provision request
        response = await self.treq.post(self._build_api_url('sensorgatewayprovisionrequests/'), json={
            'identifier': self._device_id_provider.get_device_id(),
            'signature': {
                'client_pubkey': encoding.hexlify_string(self.signature_helper.get_public_key())
            },
            'handshake': {
                'client_pubkey': encoding.hexlify_string(self.handshake_helper.get_public_key())
            },
            'metadata': self.buildMetadata()
        }, headers={'Authorization': 'Bearer {}'.format(access_token)}, agent=Agent(reactor))

        if response.code == 409:
            logger.error(
                'The device is already registered. It should be removed on the server to allow re-provisioning.')
            reactor.callLater(self.provision_retry_interval, lambda: ensureDeferred(self._provision()))
            return

        assert response.code == 201, '_provision: response code: {}, response: {}'.format(
            response.code, response)

        # Get data
        data = await json_content(response=response)

        assert data['guid'] is not None
        assert data['identifier'] is not None
        assert data['signature']['server_pubkey'] is not None
        assert data['handshake']['server_pubkey'] is not None
        assert data['handshake']['challenge'] is not None

        self._provisioning_request_guid = data['guid']

        self._provisioning_poll_loop.start(self.provisioning_state_poll_interval, now=True)

    async def _poll_provisioning_state(self):
        if self._provisioning_poll_in_progress:
            return

        with shelve.open(self._data_file) as db:
            if 'guid' in db:
                if 'challenge' in db:
                    del db['challenge']
                self._device_guid = db['guid']
                self._device_name = db['name']
                self._setDeviceReady()
                return

        logger.debug('polling for provisioning update...')
        self._provisioning_poll_in_progress = True
        assert self._provisioning_request_guid is not None

        # Get access token
        access_token = await self.get_or_request_access_token()

        # Get provisioning state
        response = await self.treq.get(
            self._build_api_url('sensorgatewayprovisionrequests/{}/'.format(self._provisioning_request_guid)),
            headers={'Authorization': 'Bearer {}'.format(access_token)}, agent=Agent(reactor))

        assert response.code == 200, '_poll_provisioning_state: response code: {}, response: {}'.format(
            response.code, response)

        # Get data
        data = await json_content(response=response)

        # Check if the gateway is deviceReady
        gateway = data.get('gateway')
        if gateway:

            # Stop the poll loop
            if self._provisioning_poll_loop and self._provisioning_poll_loop.running:
                self._provisioning_poll_loop.stop()

            # Update config
            with shelve.open(self._data_file) as db:
                if 'challenge' in db:
                    del db['challenge']
                db['guid'] = gateway['guid']
                db['name'] = gateway['name']

            self._device_guid = gateway['guid']
            self._device_name = gateway['name']

            self._setDeviceReady()
        elif not self.challenge:
            self._load_challenge(data['handshake']['challenge'], data['handshake']['server_pubkey'])
            self.onChallenge(self.challenge)

        self._provisioning_poll_in_progress = False

    def _load_challenge(self, challenge: str, server_pubkey: str) -> None:
        # The player has to be able to decrypt and display the challenge for someone to be able to see it on screen and
        #   input it into the backend to provision the player.
        encrypted_challenge = encoding.unhexlify_string(challenge)
        decrypted_challenge = self.handshake_helper.decrypt(
            encoding.unhexlify_string(server_pubkey), encrypted_challenge)
        assert len(decrypted_challenge) == 6

        # Store challenge
        with shelve.open(self._data_file) as db:
            db['challenge'] = decrypted_challenge

    def _build_api_url(self, path: str) -> str:
        return urllib.parse.urljoin(self._api_url, path)


class EkonaClientService(EkonaClient, Service):
    auto_provision = True

    def startService(self):
        Service.startService(self)

        # Provision
        if self.auto_provision:
            self.provision()

    def stopService(self):
        Service.stopService(self)

        self.stop_provisioning()
