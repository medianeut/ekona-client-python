# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import io
import json
import os
import threading
import subprocess
from datetime import datetime, timezone
from subprocess import CalledProcessError

_TWISTED = False
try:
    from twisted.internet import reactor
    from twisted.logger import Logger, FileLogObserver
    _TWISTED = True
except ImportError:
    pass


def eventAsJSON(event, transform_event=None):
    if transform_event and callable(transform_event):
        event = transform_event(event)
    return json.dumps(event) + '\n'


class AutoPrunedLogFile(io.IOBase):
    """
    Twisted only
    """
    def __init__(self, filename, max_lines=500000, prune_amount=50000):
        self._filename = os.path.abspath(filename)

        root = os.path.dirname(self._filename)
        if not os.path.isdir(root):
            os.makedirs(root)

        self._file = io.open(self._filename, 'a')

        assert prune_amount < max_lines
        self._prune_amount = prune_amount
        self._max_lines = max_lines

    def write(self, data):
        this = self

        def _write(d):
            if type(d) != str:
                d = d.decode('utf-8')
            this._file.write(d)
            this._prune()

        reactor.callFromThread(_write, data)

    def flush(self):
        reactor.callFromThread(self._file.flush)

    def _prune(self):
        num_lines = self._count()
        if num_lines > self._max_lines:
            print('prune log file.........')
            try:
                subprocess.check_output("sed -i '1,{}d' {}".format(self._prune_amount, self._filename), shell=True)
            except CalledProcessError:
                print('== could not prune log file ==')

    def _count(self):
        return int(subprocess.check_output("/usr/bin/wc -l {}".format(self._filename), shell=True).split()[0])


def jsonFileLogObserver(outFile, transform_event=None, **kwargs):
    """
    Twisted only
    """
    logFile = AutoPrunedLogFile(outFile, **kwargs)
    return FileLogObserver(
        logFile,
        lambda event: eventAsJSON(event, transform_event=transform_event)
    )


def transform_for_datadog(event):
    """
    Twisted only
    """
    result = {
        'message': event['log_format'].format(**event),
        'logger.name': event['log_namespace'],
        'logger.thread_name': threading.current_thread().name,
        'timestamp': datetime.fromtimestamp(event['log_time'], tz=timezone.utc).isoformat(),
        'level': event['log_level'].name.lower(),
    }

    fail = event.get('log_failure', None)
    if fail is not None:
        result.update({
            'error.message': fail.getErrorMessage(),
            'error.stack': fail.getTraceback(),
            'error.kind': fail.type.__name__
        })

    return result


def getLogger(namespace=None, source=None, **kwargs):
    if not _TWISTED:
        import logging
        return logging.getLogger(name=namespace)

    observer = None

    # If there is a logfile, create observer
    log_file = os.getenv('LOG_FILE', None)
    if log_file is not None:
        parameters = {
            'max_lines': kwargs.get('max_lines', 10000),
            'prune_amount': kwargs.get('prune_amount', 1000)
        }
        parameters.update(**kwargs)
        observer = jsonFileLogObserver(
            log_file, transform_event=transform_for_datadog, **parameters)

    return Logger(namespace=namespace, source=source, observer=observer)
