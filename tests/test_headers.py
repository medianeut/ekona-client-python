# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from unittest import TestCase

import datetime
from dateutil.parser import parse as dateparse

from ekona_client import headers
from .mock import MockDevice, MockDeviceLibraryRegistry


class DeviceHeaderTamperTests(TestCase):
    def setUp(self):
        self.device = MockDevice(device_type='sensor-gateway')

        self.content = {'a': 1, 'b': []}

        self.host = 'test.nurama.com'
        self.path = '/test/'
        self.query = 'a=15'
        self.body = json.dumps(self.content)
        self.content_type = 'application/json; charset=utf-8'

        self.device_headers = headers.get_device_headers(
            device_type=self.device.device_type, device_guid=self.device.device_guid,
            device_signature_helper=self.device.signature_helper,
            host=self.host, path=self.path, query=self.query, body=self.body,
            content_type=self.content_type)

        assert isinstance(dateparse(self.device_headers['Date']), datetime.datetime)
        assert self.device_headers['X-EkonaDevice-Id'] == str(self.device.device_guid)
        assert self.device_headers['X-EkonaDevice-Type'] == self.device.device_type
        assert self.device_headers['X-EkonaDevice-Authorization'] is not None

        self.device_library = MockDeviceLibraryRegistry()
        self.device_library.get(self.device.device_type).add(self.device)

    def test_device_headers_no_tampering(self):
        result = headers.verify_device_authorization(
            device_registry=self.device_library,
            device_type=self.device.device_type,
            device_guid=self.device.device_guid,
            device_auth_signature=self.device_headers['X-EkonaDevice-Authorization'],
            request_timestamp_str=self.device_headers['Date'],
            host=self.host, path=self.path, query=self.query, body=self.body,
            content_type=self.content_type)

        assert isinstance(result, MockDevice)
        assert result.device_guid == self.device.device_guid

    def test_device_headers_tamper_host(self):
        result = headers.verify_device_authorization(
            device_registry=self.device_library,
            device_type=self.device.device_type,
            device_guid=self.device.device_guid,
            device_auth_signature=self.device_headers['X-EkonaDevice-Authorization'],
            request_timestamp_str=self.device_headers['Date'],
            host='some-other.nurama.com', path=self.path, query=self.query, body=self.body,
            content_type=self.content_type)

        assert not result

    def test_device_headers_tamper_path(self):
        result = headers.verify_device_authorization(
            device_registry=self.device_library,
            device_type=self.device.device_type,
            device_guid=self.device.device_guid,
            device_auth_signature=self.device_headers['X-EkonaDevice-Authorization'],
            request_timestamp_str=self.device_headers['Date'],
            host=self.host, path='/some-other-path/', query=self.query, body=self.body,
            content_type=self.content_type)

        assert not result

    def test_device_headers_tamper_query(self):
        result = headers.verify_device_authorization(
            device_registry=self.device_library,
            device_type=self.device.device_type,
            device_guid=self.device.device_guid,
            device_auth_signature=self.device_headers['X-EkonaDevice-Authorization'],
            request_timestamp_str=self.device_headers['Date'],
            host=self.host, path=self.path, query='b=2', body=self.body,
            content_type=self.content_type)

        assert not result

    def test_device_headers_tamper_body(self):
        result = headers.verify_device_authorization(
            device_registry=self.device_library,
            device_type=self.device.device_type,
            device_guid=self.device.device_guid,
            device_auth_signature=self.device_headers['X-EkonaDevice-Authorization'],
            request_timestamp_str=self.device_headers['Date'],
            host=self.host, path=self.path, query=self.query, body='blabla',
            content_type=self.content_type)

        assert not result
