# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

import pytest
from unittest import TestCase

from ekona_client.crypto import KeyDerivationHelper, HandshakeHelper, SignatureHelper, InvalidSignature


class KeyDerivationTests(TestCase):
    def test_basic_key_derivation(self):
        challenge = 'abc123'.encode('utf-8')

        kdf_helper = KeyDerivationHelper()

        signature = kdf_helper.derive(challenge)

        kdf_helper2 = KeyDerivationHelper()
        kdf_helper2.verify(challenge, signature)


class HandshakeTests(TestCase):
    def test_key_exchange(self):
        """
        This tests a key exchange system where
        """
        # CLIENT WANTS TO PERFORM KEY EXCHANGE

        client_handshake = HandshakeHelper()
        client_public_key_string = client_handshake.get_public_key()

        # SEND PUBLIC KEY AS STRING TO SERVER TO INITIATE KEY EXCHANGE

        server_handshake = HandshakeHelper()
        server_public_key_string = server_handshake.get_public_key()

        # SERVER ENCRYPTS CHALLENGE
        server_challenge = 'abc123'.encode('utf-8')
        encrypted_server_challenge = server_handshake.encrypt(
            client_public_key_string, server_challenge)

        # CLIENT DECRYPTS CHALLENGE
        decrypted_challenge = client_handshake.decrypt(
            server_public_key_string, encrypted_server_challenge)

        assert decrypted_challenge == server_challenge

    def test_encrypted_data_uniqueness(self):
        """
        This tests that data encrypted with the same symmetric key is never the same
        """
        client_handshake = HandshakeHelper()
        client_public_key_string = client_handshake.get_public_key()

        server_handshake = HandshakeHelper()

        plain_data = 'abc123'.encode('utf-8')

        test_data = []
        while len(test_data) < 10:
            encrypted_data = server_handshake.encrypt(
                client_public_key_string, plain_data)
            assert encrypted_data not in test_data
            test_data.append(encrypted_data)


class SignatureTests(TestCase):
    def test_tampered_signature(self):
        plain_data = 'abc123'.encode('utf-8')

        server_signature = SignatureHelper()
        signature = server_signature.sign(plain_data)
        server_public_key = server_signature.get_public_key()

        # TAMPER WITH SIGNATURE
        signature = signature[5:]
        signature += os.urandom(5)

        # SERVER SENDS PUBKEY AND SIGNATURE TO CLIENT

        with pytest.raises(InvalidSignature):
            SignatureHelper.verify(server_public_key, signature, plain_data)

    def test_tampered_data(self):
        plain_data = 'abc123'.encode('utf-8')

        server_signature = SignatureHelper()
        signature = server_signature.sign(plain_data)
        server_public_key = server_signature.get_public_key()

        # TAMPER WITH DATA
        plain_data = 'def456'.encode('utf-8')

        # SERVER SENDS PUBKEY AND SIGNATURE TO CLIENT

        with pytest.raises(InvalidSignature):
            SignatureHelper.verify(server_public_key, signature, plain_data)

    def test_correct_signature(self):
        plain_data = 'abc123'.encode('utf-8')

        server_signature = SignatureHelper()
        signature = server_signature.sign(plain_data)
        server_public_key = server_signature.get_public_key()

        # SERVER SENDS PUBKEY AND SIGNATURE TO CLIENT

        SignatureHelper.verify(server_public_key, signature, plain_data)

    def test_serialize_without_password(self):
        plain_data = 'abc123'.encode('utf-8')

        server_signature = SignatureHelper()
        signature = server_signature.sign(plain_data)

        server_private_key = server_signature.get_private_key()

        # E.G. SAVE TO STORAGE, LOAD AGAIN AND USE IT TO VERIFY

        server_signature_2 = SignatureHelper.load(server_private_key)
        SignatureHelper.verify(server_signature_2.get_public_key(), signature, plain_data)

    def test_serialize_with_password(self):
        plain_data = 'abc123'.encode('utf-8')
        password = b'my-password'

        server_signature = SignatureHelper()
        signature = server_signature.sign(plain_data)

        server_private_key = server_signature.get_private_key(password=password)

        # E.G. SAVE TO STORAGE, LOAD AGAIN AND USE IT TO VERIFY

        server_signature_2 = SignatureHelper.load(server_private_key, password=password)
        SignatureHelper.verify(server_signature_2.get_public_key(), signature, plain_data)
