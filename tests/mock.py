# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from ekona_client import crypto
from ekona_client.devices import AbstractDeviceMixin, AbstractDeviceIntegration, AbstractDeviceLibraryRegistry


class MockDevice(AbstractDeviceMixin, object):
    def __init__(self, device_type: str, device_guid: uuid.UUID=None, signature_helper: crypto.SignatureHelper=None,
                 active: bool=True):
        self.device_type = device_type
        self.device_guid = device_guid or uuid.uuid4()
        self.signature_helper = signature_helper or crypto.SignatureHelper()
        self.active = active

    def is_active(self) -> bool:
        return self.active

    def get_signature_public_key(self) -> bytes:
        return self.signature_helper.get_public_key()


class MockDeviceIntegration(AbstractDeviceIntegration):
    def __init__(self, device_type):
        self.device_type = device_type
        self.devices = {}

    def add(self, device: MockDevice) -> MockDevice:
        self.devices[device.device_guid] = device
        return device

    def remove(self, device: MockDevice) -> MockDevice:
        if device.device_guid in self.devices:
            del self.devices[device.device_guid]
        return device

    def get_device(self, guid: uuid.UUID) -> MockDevice:
        return self.devices[guid]

    def get_device_factory_class(self) -> object:
        raise NotImplementedError


class MockDeviceLibraryRegistry(AbstractDeviceLibraryRegistry):
    def __init__(self):
        self.data = {}

    def get(self, device_type: str) -> MockDeviceIntegration:
        if device_type not in self.data:
            self.data[device_type] = MockDeviceIntegration(device_type)
        return self.data[device_type]
