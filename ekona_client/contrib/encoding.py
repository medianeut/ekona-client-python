# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
import binascii


def base64_encode_string(data):
    return base64.encodebytes(data).decode('ascii')


def base64_decode_string(data):
    return base64.decodebytes(data.encode('ascii'))


def hexlify_string(data):
    return binascii.hexlify(data).decode('utf-8')


def unhexlify_string(data):
    return binascii.unhexlify(data.encode('utf-8'))
