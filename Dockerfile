FROM ditchitall/pyenv:latest

COPY ./ /src

RUN set -x \
    && find /src -type f -name "*.py[co]" -delete \
    && find /src -type f -name "__pycache__" -delete

WORKDIR /src

CMD ["tox"]
