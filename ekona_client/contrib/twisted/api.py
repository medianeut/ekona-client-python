# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import treq
from urllib.parse import urlparse

from ekona_client.headers import get_device_headers
from ekona_client.contrib.logs import getLogger

from .client import EkonaClient

logger = getLogger(__name__)


class EkonaClientTreqWrapper(object):
    def __init__(self, client: EkonaClient):
        self._client = client

    async def head(self, url, **kwargs):
        """
        Make a ``HEAD`` request.

        See :py:func:`treq.request`
        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('head: url={url}, headers={headers}, kwargs={kwargs}',
                     url=url, headers=headers, kwargs=kwargs)
        return await treq.head(url, headers=headers, **kwargs)

    async def get(self, url, headers=None, **kwargs):
        """
        Make a ``GET`` request.

        See :py:func:`treq.request`
        """
        headers = await self._inject_auth_headers(url=url, headers=headers, **kwargs)
        logger.debug('get: url={url}, headers={headers}, kwargs={kwargs}',
                     url=url, headers=headers, kwargs=kwargs)
        return await treq.get(url, headers=headers, **kwargs)

    async def post(self, url, data=None, **kwargs):
        """
        Make a ``POST`` request.

        See :py:func:`treq.request`
        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('post: url={url}, data={data}, headers={headers}, kwargs={kwargs}',
                     url=url, data=data, headers=headers, kwargs=kwargs)
        return await treq.post(url, data=data, headers=headers, **kwargs)

    async def put(self, url, data=None, **kwargs):
        """
        Make a ``PUT`` request.

        See :py:func:`treq.request`
        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('put: url={url}, data={data}, headers={headers}, kwargs={kwargs}',
                     url=url, data=data, headers=headers, kwargs=kwargs)
        return await treq.put(url, data=data, headers=headers, **kwargs)

    async def patch(self, url, data=None, **kwargs):
        """
        Make a ``PATCH`` request.

        See :py:func:`treq.request`
        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('patch: url={url}, data={data}, headers={headers}, kwargs={kwargs}',
                     url=url, data=data, headers=headers, kwargs=kwargs)
        return await treq.patch(url, data=data, headers=headers, **kwargs)

    async def delete(self, url, **kwargs):
        """
        Make a ``DELETE`` request.

        See :py:func:`treq.request`
        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('get: url={url}, headers={headers}, kwargs={kwargs}',
                     url=url, headers=headers, kwargs=kwargs)
        return await treq.delete(url, headers=headers, **kwargs)

    async def request(self, method, url, **kwargs):
        """
        Make an HTTP request.

        :param str method: HTTP method. Example: ``'GET'``, ``'HEAD'``. ``'PUT'``,
             ``'POST'``.
        :param str url: http or https URL, which may include query arguments.

        :param headers: Optional HTTP Headers to send with this request.
        :type headers: Headers or None

        :param params: Optional parameters to be append as the query string to
            the URL, any query string parameters in the URL already will be
            preserved.

        :type params: dict w/ str or list/tuple of str values, list of 2-tuples, or
            None.

        :param data: Optional request body.
        :type data: str, file-like, IBodyProducer, or None

        :param json: Optional JSON-serializable content to pass in body.
        :type json: dict, list/tuple, int, string/unicode, bool, or None

        :param reactor: Optional twisted reactor.

        :param bool persistent: Use persistent HTTP connections.  Default: ``True``
        :param bool allow_redirects: Follow HTTP redirects.  Default: ``True``

        :param auth: HTTP Basic Authentication information.
        :type auth: tuple of ``('username', 'password')``.

        :param cookies: Cookies to send with this request.  The HTTP kind, not the
            tasty kind.
        :type cookies: ``dict`` or ``cookielib.CookieJar``

        :param int timeout: Request timeout seconds. If a response is not
            received within this timeframe, a connection is aborted with
            ``CancelledError``.

        :param bool browser_like_redirects: Use browser like redirects
            (i.e. Ignore  RFC2616 section 10.3 and follow redirects from
            POST requests).  Default: ``False``

        :param bool unbuffered: Pass ``True`` to to disable response buffering.  By
            default treq buffers the entire response body in memory.

        :rtype: Deferred that fires with an IResponse provider.

        """
        initial_headers = kwargs.pop('headers', {})
        headers = await self._inject_auth_headers(url=url, headers=initial_headers, **kwargs)
        logger.debug('request: method={method}, url={url}, headers={headers}, kwargs={kwargs}',
                     method=method, url=url, headers=headers, kwargs=kwargs)
        return await treq.request(method, url, headers=headers, **kwargs)

    async def _inject_auth_headers(self, url: str, headers: dict=None, **kwargs) -> dict:
        headers = headers or {}

        if 'auth' in kwargs:
            return headers

        # Make sure we have a content type
        headers['Content-Type'] = headers.get('Content-Type', 'text/plain')

        access_token = await self._client.get_or_request_access_token()

        if not access_token:
            logger.warn('_inject_auth_headers: could not retrieve or request access token!')
            return headers

        logger.debug('_inject_auth_headers: access_token: {access_token}, device_guid: {device_guid}',
                     access_token=access_token, device_guid=self._client.device_guid)

        # If the client is not yet provisioned, return the standard headers
        if not self._client.device_guid:
            logger.debug('_inject_auth_headers: not yet provisioned. Omitting auth headers...')
            return headers

        logger.debug('_inject_auth_headers: provisioning complete. Adding auth headers...')

        # Prepare body
        body = kwargs.get('body', '')

        result = urlparse(url)
        host = result.hostname
        path = result.path
        query = result.query

        headers.update({
            'Authorization': 'Bearer {}'.format(access_token)
        })

        headers.update(get_device_headers(
            device_type=self._client.device_type,
            device_guid=self._client.device_guid,
            device_signature_helper=self._client.signature_helper,
            host=host,
            path=path,
            query=query,
            body=body,
            content_type=headers['Content-Type']))

        return headers

