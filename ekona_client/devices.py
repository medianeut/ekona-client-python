# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from abc import abstractmethod, abstractproperty

import six
import abc

import uuid


class AbstractDeviceMixin(object):
    def is_active(self):
        raise NotImplementedError

    def get_signature_public_key(self):
        raise NotImplementedError


class AbstractDeviceIntegration(six.with_metaclass(abc.ABCMeta, object)):
    @abstractmethod
    def get_device(self, guid: uuid.UUID) -> AbstractDeviceMixin:
        pass

    @abstractmethod
    def get_device_factory_class(self) -> object:
        pass


class AbstractDeviceLibraryRegistry(six.with_metaclass(abc.ABCMeta, object)):
    @abstractmethod
    def get(self, device_type: str) -> AbstractDeviceIntegration:
        pass
