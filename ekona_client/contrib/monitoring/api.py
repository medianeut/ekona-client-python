# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

import time
import functools

from ekona_client import settings

from datadog import api as dd_api, statsd as dd_statsd
from datadog import initialize as dd_init


from raven import Client as _SentryClient
sentry_client = _SentryClient(settings.SENTRY_DSN)

DOGSTATSD = False

dd_config = {
    'host_name': settings.DATADOG_HOST_NAME,
}

if settings.DATADOG_API_KEY is not None:
    dd_config.update({
        'api_key': settings.DATADOG_API_KEY,
        'app_key': settings.DATADOG_APP_KEY,
    })

if settings.DATADOG_STATSD_HOST is not None:
    DOGSTATSD = True
    dd_config.update({
        'statsd_host': settings.DATADOG_STATSD_HOST,
        'statsd_port': settings.DATADOG_STATSD_PORT
    })

dd_init(**dd_config)


# def get_resin_tags():
#     return {
#         # These match platform variables
#         'resin_uuid': os.getenv('RESIN_DEVICE_UUID', ''),
#         'device_type': os.getenv('RESIN_DEVICE_TYPE', ''),
#         'device_name': os.getenv('RESIN_DEVICE_NAME_AT_INIT', ''),
#
#         # Other
#         'resin_app_id': os.getenv('RESIN_APP_ID', ''),
#         'resin_app_name': os.getenv('RESIN_APP_NAME', ''),
#         'resin_supervisor_version': os.getenv('RESIN_SUPERVISOR_VERSION', ''),
#         'resin_host_os_version': os.getenv('RESIN_HOST_OS_VERSION', '')
#     }


def get_dd_tags(**tags):
    tags.update({
        'env': settings.ENVIRONMENT
    })
    # tags['resin'] = os.getenv('RESIN', '0') == '1'
    # if tags['resin']:
    #     tags.update(get_resin_tags())
    tag_list = []
    for k, v in tags.items():
        tag_list.append('{}:{}'.format(k, v))
    return tag_list


if settings.DATADOG_API_KEY is not None:
    # Update default tags for host
    dd_api.Tag.update(settings.DATADOG_HOST_NAME, tags=get_dd_tags())


class MonitorDuration(object):
    def __init__(self, name, **tags):
        self.name = name
        self.tags = tags
        self.start_time = None
        self.end_time = None

    def __enter__(self):
        self.start_time = time.time()
        return self

    def __exit__(self, exc, value, tb):
        if not self.start_time:
            return
        self.end_time = time.time()
        duration = self.end_time - self.start_time

        if DOGSTATSD:
            dd_statsd.gauge(metric=self.name, value=duration, tags=get_dd_tags(**self.tags))
        elif settings.DATADOG_API_KEY is not None:
            dd_api.Metric.send(metric=self.name, points=[duration], type='gauge', tags=get_dd_tags(**self.tags))


def monitor_duration(name):
    def _decorator(f):
        @functools.wraps(f)
        def _wrapper(*args, **kwargs):
            with MonitorDuration(name):
                return f(*args, **kwargs)
        return _wrapper
    return _decorator


def gauge(name, value, **tags):
    if DOGSTATSD:
        dd_statsd.gauge(metric=name, value=value, tags=get_dd_tags(**tags))
    elif settings.DATADOG_API_KEY is not None:
        dd_api.Metric.send(metric=name, points=[value], type='gauge', tags=get_dd_tags(**tags))


def increment(name, value=1, **tags):
    if DOGSTATSD:
        dd_statsd.increment(metric=name, value=value, tags=get_dd_tags(**tags))
    elif settings.DATADOG_API_KEY is not None:
        dd_api.Metric.send(metric=name, points=[value], type='counter', tags=get_dd_tags(**tags))


def report_event(title, text, **tags):
    if DOGSTATSD:
        dd_statsd.event(title=title, text=text, tags=get_dd_tags(**tags))
    elif settings.DATADOG_API_KEY is not None:
        dd_api.Event.create(title=title, text=text, tags=get_dd_tags(**tags))
