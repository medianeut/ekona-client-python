# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

ENVIRONMENT = os.getenv('ENVIRONMENT', 'production')

DEFAULT_CONTENT_TYPE = 'application/octet-stream'
DEFAULT_SIGNED_HEADER_SPLIT_CHAR = ':'
DEFAULT_DATE_HEADER_TIMESTAMP_FMT = '%a, %d %b %Y %H:%M:%S %Z'

USE_TZ = os.getenv('USE_TZ', True)

SIGNED_HEADER_SPLIT_CHAR = os.getenv('SIGNED_HEADER_SPLIT_CHAR', DEFAULT_SIGNED_HEADER_SPLIT_CHAR)
DATE_HEADER_TIMESTAMP_FMT = os.getenv('DATE_HEADER_TIMESTAMP_FMT', DEFAULT_DATE_HEADER_TIMESTAMP_FMT)

# Datadog
DATADOG_API_KEY = os.getenv('DATADOG_API_KEY', None)
DATADOG_APP_KEY = os.getenv('DATADOG_APP_KEY', None)
DATADOG_HOST_NAME = os.getenv('RESIN_DEVICE_UUID', os.getenv('DATADOG_HOST_NAME', None))
DATADOG_STATSD_HOST = os.getenv('DATADOG_STATSD_HOST', None)
DATADOG_STATSD_PORT = int(os.getenv('DATADOG_STATSD_PORT', 8125))

# Sentry: https://sentry.io/nurama/ekona-gateway
SENTRY_DSN = os.getenv('SENTRY_DSN', None)
SENTRY_ENVIRONMENT = os.getenv('SENTRY_ENVIRONMENT', ENVIRONMENT)
