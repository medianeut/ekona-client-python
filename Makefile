ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

LOCAL_USER_ID=$(shell id -u)

GIT_COMMIT_HASH=$(shell git rev-parse --verify HEAD)

.PHONY: create_env
create_env:
	echo LOCAL_USER_ID=${LOCAL_USER_ID} > ${ROOT_DIR}/.env
	echo LOCAL_USERNAME=${USER} >> ${ROOT_DIR}/.env
	echo GIT_COMMIT_HASH=${GIT_COMMIT_HASH} >> ${ROOT_DIR}/.env

.PHONY: build
build: create_env
	docker-compose build

.PHONY: test
test: create_env
	docker-compose run --rm tox tox
