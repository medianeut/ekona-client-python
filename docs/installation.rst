============
Installation
============

At the command line::

    $ easy_install ekona-client

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv ekona-client
    $ pip install ekona-client
